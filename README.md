# Super Agents

### Table of Contents
* [What is this?](#what-is-this)
* [Features](#features)
* [Requirements](#requirements)
* [Installation](#installation)


## What is this?
This application allows you to search for a list of agents, whose content comes from http://akurey.com/trainings/companies.json

## Features
Containing:
- This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.2.
- SCSS
- Dependency loading and startup with [npm](http://npmjs.com)
- Better IDE support with [tshin] 

## Requirements
You need [node.js](http://nodejs.org) with [npm](http://npmjs.com) on your machine.

## Installation
This app will install all required dependencies automatically. 
Just start the commands below in the root folder where you stored the package.json file
```SH
$ npm install
```
## Install Angular CLI 
Install Angular CLI by running the following command:
```SH
$ npm i -g @angular/cli
```
## Development server

For the first time run the `npm start` to enable the proxy configured in the package.json , the rest of the time you can use  `ng serve` for a dev server. Navigate to `http://localhost:4200/`  or run `ng server` -o to open automatically the browser and use the application.
The app will automatically reload if you change any of the source files.

 "start": "ng serve --proxy-config proxyconfig.json",


