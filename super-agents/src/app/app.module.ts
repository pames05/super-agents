import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule, Routes} from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AgentComponent } from './components/agent/agent.component';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { HomeService } from './services/home/home.service';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule } from '@angular/forms';
const appRoutes: Routes = [{path: '**', component: HomeComponent}];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AgentComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    NgxPaginationModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  providers: [HomeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
