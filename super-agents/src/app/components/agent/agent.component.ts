import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-agent',
  templateUrl: './agent.component.html',
  styleUrls: ['./agent.component.scss']
})
export class AgentComponent implements OnInit {
  // Get agent from the list
  @Input () agent: any = {};
  constructor() { }

  ngOnInit() {
    // console.log(this.agent);
  }

}
