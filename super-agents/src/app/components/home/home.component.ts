import { Component, OnInit } from '@angular/core';
import { HomeService } from '../../services/home/home.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  agents: any = [];
  agentsFiltered: any = [];
  angentsLength: number;
  searchInput: string;
  constructor(public homeService: HomeService) {
    this.searchInput = '';
  }

  ngOnInit() {
   this.angentsLength = 0;
   // Call service
   this.homeService.getSuperAgents().then(data => {
     // Fill agents list
      this.agents = data;
      this.agentsFiltered.companies = this.agents.companies.slice();
      this.angentsLength = this.agents.companies.length;
    }).catch(error => {
      // Promise error: this.angentsLength will remain zero
      console.log(error);
    });
  }

  autoSearch(searchInput) {
    if (searchInput.trim().length >= 3) {
          this.search(searchInput);
    } else if (searchInput.trim() === '') {
      this.agentsFiltered.companies = this.agents.companies.slice();
    }

  }

  // Search event for filtered companies list
  search(searchInput) {
    // Fill this.agentsFiltered.companies
    this.agentsFiltered.companies = this.agents.companies.slice();
    // Filter by searchInput
    if ( searchInput.trim() !== '') {
         this.agentsFiltered.companies = this.agentsFiltered.companies.filter(function(item) {
        return  item.name.toLowerCase().indexOf(searchInput.toLowerCase()) > -1;
      });
    }
  }
}
