
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http , HttpModule, Response } from '@angular/http';

@Injectable()
export class HomeService {
  results: any;
  constructor(public http: Http) {
    this.results = [];
   }
  getSuperAgents() {
    // create promise to call endpoint
    const promise = new Promise((resolve, reject) => {
    // const url = 'companies'; // url proxy in proxyconfig.json
     const url = 'https://api.myjson.com/bins/uptto';
      this.http.get(url)
      .toPromise()
        .then(
          res => { // Success
              this.results = res.json();
              resolve(this.results);
          },
          msg => {
            reject ('error');
          }
        );
    });
     return promise;
  }
}
